"use strict";

// Scrolling
var scrolled = false;
var scrollYPos = 0;
var header = document.getElementsByTagName('header')[0];
var headerHeight = header.offsetHeight;

var scrollHandler = function scrollHandler() {
  var scrollTop = window.scrollY;

  if (scrollTop > scrollYPos) {
    header.classList.remove('on-top');

    if (scrollTop > headerHeight) {
      header.classList.remove('on-top');
      header.classList.add('scrolled-down');
    }
  } else if (scrollTop + window.innerHeight < document.body.clientHeight) {
    header.classList.remove('scrolled-down');
    header.classList.add('on-top');
  }

  if (scrollTop === 0) {
    header.classList.remove('on-top');
  }

  scrollYPos = scrollTop;
};

window.addEventListener('scroll', function () {
  scrollHandler();
});
window.addEventListener('load', function () {
  scrollHandler();
});
window.setInterval(function () {
  if (scrolled) {
    scrollHandler();
    scrolled = false;
  }
}, 250); // Burger menu

var nav = document.getElementById('nav');
var burger = document.getElementById('burger');
var closebtn = document.getElementById('close-btn');

var burgerClick = function burgerClick() {
  nav.classList.add('show');
};

var closeClick = function closeClick() {
  nav.classList.remove('show');
};

burger.addEventListener('click', burgerClick);
closebtn.addEventListener('click', closeClick);