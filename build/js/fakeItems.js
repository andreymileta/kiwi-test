"use strict";

var servicesInfoContent = document.querySelector('.services-info .content').children;
var fakeItem = document.createElement('div');
fakeItem.classList.add('fake-item');

for (var i = 0; i < servicesInfoContent.length; i++) {
  for (var k = 0; k < 3; k++) {
    servicesInfoContent[i].append(fakeItem);
  }
}