"use strict";

window.onload = function () {
  var servicesNavigationLi = document.querySelector('.services-info .navigation').children;

  for (var i = 0; i < servicesNavigationLi.length; i++) {
    servicesNavigationLi[i].children[0].addEventListener('click', navClickHandler);
  }
};

var navClickHandler = function navClickHandler(event) {
  event.preventDefault();
  var anchor = event.target;
  var allNavigationLi = document.querySelector('.services-info .navigation').children;
  var allServicesItems = document.querySelector('.services-info .content').children; // Romove 'active' class from all LI elements in navigation section

  for (var i = 0; i < allNavigationLi.length; i++) {
    allNavigationLi[i].classList.remove('active');
  } // Remove 'show' class from all items in content section


  for (var _i = 0; _i < allServicesItems.length; _i++) {
    allServicesItems[_i].classList.remove('show');
  }

  var foundedSection = document.querySelector(anchor.getAttribute('href'));
  anchor.parentNode.classList.add('active');
  foundedSection.classList.add('show');
};