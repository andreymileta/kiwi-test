// Scrolling

let scrolled = false;
let scrollYPos = 0;

const header = document.getElementsByTagName('header')[0];
const headerHeight = header.offsetHeight;

const scrollHandler = () => {
	let scrollTop = window.scrollY;

	if (scrollTop > scrollYPos) {
		header.classList.remove('on-top');
		if (scrollTop > headerHeight) {
			header.classList.remove('on-top');
			header.classList.add('scrolled-down');
		}
	} else if (scrollTop + window.innerHeight < document.body.clientHeight) {
		header.classList.remove('scrolled-down');
		header.classList.add('on-top');
	}
	if (scrollTop === 0) {
		header.classList.remove('on-top');
	}

	scrollYPos = scrollTop;
};

window.addEventListener('scroll', () => {
	scrollHandler();
});

window.addEventListener('load', () => {
	scrollHandler();
});

window.setInterval(() => {
	if (scrolled) {
		scrollHandler();
		scrolled = false;
	}
}, 250);

// Burger menu

const nav = document.getElementById('nav');
const burger = document.getElementById('burger');
const closebtn = document.getElementById('close-btn');

const burgerClick = () => {
	nav.classList.add('show');
};
const closeClick = () => {
	nav.classList.remove('show');
};

burger.addEventListener('click', burgerClick);
closebtn.addEventListener('click', closeClick);
