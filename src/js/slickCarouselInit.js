// import $ from 'jquery';

$(document).ready(function() {
	$('.carousel').slick({
		dots: true,
		infinite: true,
		speed: 300,
		adaptiveHeight: true,
		prevArrow: $('.prev'),
		nextArrow: $('.next')
	});
	$('.providers-carousel').slick({
		dots: false,
		infinite: true,
		speed: 300,
		adaptiveHeight: true,
		slidesToShow: 2,
		slidesToScroll: 1,
		prevArrow: $('.providers .prev'),
		nextArrow: $('.providers .next'),
		responsive: [
			{
				breakpoint: 979,
				settings: {
					slidesToShow: 1
				}
			}
		]
	});
});
