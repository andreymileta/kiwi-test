const servicesInfoContent = document.querySelector('.services-info .content')
	.children;

const fakeItem = document.createElement('div');
fakeItem.classList.add('fake-item');

for (let i = 0; i < servicesInfoContent.length; i++) {
	for (let k = 0; k < 3; k++) {
		servicesInfoContent[i].append(fakeItem);
	}
}
