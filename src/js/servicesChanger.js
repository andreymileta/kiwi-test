window.onload = () => {
	const servicesNavigationLi = document.querySelector(
		'.services-info .navigation'
	).children;

	for (let i = 0; i < servicesNavigationLi.length; i++) {
		servicesNavigationLi[i].children[0].addEventListener(
			'click',
			navClickHandler
		);
	}
};

const navClickHandler = event => {
	event.preventDefault();

	const anchor = event.target;

	const allNavigationLi = document.querySelector('.services-info .navigation')
		.children;
	const allServicesItems = document.querySelector('.services-info .content')
		.children;

	// Romove 'active' class from all LI elements in navigation section
	for (let i = 0; i < allNavigationLi.length; i++) {
		allNavigationLi[i].classList.remove('active');
	}

	// Remove 'show' class from all items in content section
	for (let i = 0; i < allServicesItems.length; i++) {
		allServicesItems[i].classList.remove('show');
	}

	const foundedSection = document.querySelector(anchor.getAttribute('href'));

	anchor.parentNode.classList.add('active');
	foundedSection.classList.add('show');
};
