const gulp = require('gulp');
const sass = require('gulp-sass');
const browserSync = require('browser-sync').create();
const autoprefixer = require('gulp-autoprefixer');
const spritesmith = require('gulp.spritesmith');
const gulpif = require('gulp-if');
const babel = require('gulp-babel');
const rimraf = require('gulp-rimraf');
const svgSprite = require('gulp-svg-sprite');

function scss() {
	return gulp
		.src('./src/scss/**/*.scss')
		.pipe(sass())
		.pipe(autoprefixer({ cascade: true }))
		.pipe(gulp.dest('./src/css'))
		.pipe(browserSync.stream());
}

function sprites() {
	return gulp
		.src('./src/img/sprite/*.png')
		.pipe(
			spritesmith({
				imgName: 'sprite.png',
				cssName: 'sprite.scss',
				imgPath: './src/img/sprite.png'
			})
		)
		.pipe(gulpif('*.png', gulp.dest('./src/img/')))
		.pipe(gulpif('*.scss', gulp.dest('./src/scss')));
}

function createsvg() {
	return gulp
		.src('./src/img/sprite/*.svg')
		.pipe(
			svgSprite({
				mode: {
					stack: {
						sprite: '../sprite.svg'
					}
				}
			})
		)
		.pipe(gulp.dest('./src/img/'));
}

function css() {
	return gulp.src('./src/css/*').pipe(gulp.dest('./build/css'));
}

function html() {
	return gulp.src('./src/*.html').pipe(gulp.dest('./build'));
}

function fonts() {
	return gulp.src('./src/fonts/**/*/*.*').pipe(gulp.dest('./build/fonts'));
}

function images() {
	return gulp.src('./src/img/**/*.png').pipe(gulp.dest('./build/img'));
}

function js() {
	return gulp
		.src('src/js/*.js', { allowEmpty: true })
		.pipe(babel())
		.pipe(gulp.dest('./build/js'));
}

function clean() {
	return gulp.src('./build/*', { read: false }).pipe(rimraf());
}

function build(done) {
	gulp.series(html, css, js, fonts, images);
	done();
}

function watch() {
	browserSync.init({
		server: {
			baseDir: './src'
		},
		notify: false
	});

	gulp.watch('./src/scss/**/*.scss', scss);
	gulp.watch('./src/img/**/*.png', sprites);
	gulp.watch('./src/*.html', html).on('change', browserSync.reload);
	gulp.watch('./src/js/**/*.js', js).on('change', browserSync.reload);
}

exports.createsvg = createsvg;
exports.build = build;
exports.clean = clean;
exports.sprites = sprites;
exports.watch = watch;

// SCSS in CSS
// gulp.task('sass', function() {
// 	return gulp
// 		.src('src/scss/main.scss')
// 		.pipe(wait(500))
// 		.pipe(sass())
// 		.pipe(autoprefixer({ cascade: true }))
// 		.pipe(gulp.dest('src/css/'))
// 		.pipe(browserSync.reload({ stream: true }));
// });

// // Sprites
// gulp.task('sprites', function() {
// 	return gulp
// 		.src('./src/img/sprite/*.png')
// 		.pipe(
// 			spritesmith({
// 				imgName: 'sprite.png',
// 				cssName: 'sprite.scss',
// 				imgPath: '../img/sprite.png'
// 			})
// 		)
// 		.pipe(gulpif('*.png', gulp.dest('./src/img/')))
// 		.pipe(gulpif('*.scss', gulp.dest('./src/scss/')));
// });

// // Browser Sync
// gulp.task('browser-sync', function() {
// 	browserSync.init({
// 		server: {
// 			baseDir: './src'
// 		},
// 		notify: false
// 	});
// });

// // Watcher
// gulp.task(
// 	'watch',
// 	gulp.parallel(['browser-sync', 'sass', 'sprites'], function() {
// 		gulp.watch('src/scss/*.scss', ['sass']);
// 		gulp.watch('src/*.html', browserSync.reload);
// 		gulp.watch('src/js/main.js', browserSync.reload);
// 	})
// );

// // Clear cache
// gulp.task('clear', function() {
// 	return caches.clearAll();
// });

// // Default
// gulp.task(
// 	'default',
// 	gulp.series('watch', function() {
// 		gulp.src('js/app.js')
// 			.pipe(
// 				babel({
// 					presets: ['@babel/env']
// 				})
// 			)
// 			.pipe(gulp.dest('dist'));
// 	})
// );

// gulp.task('build', function() {
// 	const buildHTML = gulp.src('src/*.html').pipe(gulp.dest('build'));

// 	const buildCSS = gulp.src('src/css/*.css').pipe(gulp.dest('build/css'));

// 	const buildJS = gulp.src('src/js/**/*/*.js').pipe(gulp.dest('build/js'));

// 	const buildFonts = gulp
// 		.src('src/fonts/**/*/*.*')
// 		.pipe(gulp.dest('build/fonts'));

// 	const buildImg = gulp.src('src/img/**/*.png').pipe(gulp.dest('build/img'));
// });
